# Iotivity_framework README

1. Clone this repository.
	Run "git clone https://smartron-narayanamurthy@bitbucket.org/thome-iotframework/iotivity_framework.git"
2. Run "scons" for "resource/examples"  folder. Below is the command.
	scons resource/examples
	or
	go to <iotivity path>/resource/examples and run scons
3. goto iotivity_client/Debug folder 
4. Run "make clean" and "make"
5. you will get "iotivity_client" executable file.Run "iotivity_client".
