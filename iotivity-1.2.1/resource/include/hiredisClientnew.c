#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "hiredis.h"
/*#include <structure.c> //Our Defined structure */
using namespace std;
using namespace OC;

redisContext *c2;
redisReply *reply;
const char *hostname = "192.168.0.114";
int port = 6379;
std::string space = " ";
std::vector<int> threearray;
std::vector<int> chromargbArr;


class REDIS
{
public:
void Context(void);
void Writeredisget(std::string hash_key, const OCRepresentation& rep );
void Writeredisdiscovery(std::string hash_key, std::string discoveryinfo);
};

void REDIS::Context(void)
{
/*  struct timeval timeout = { 1, 500000 }; // 1.5 seconds*/
c2= redisConnect(hostname, port /*,timeout*/);
if (c2 == NULL || c2->err)
{
if (c2)
{
	printf("Connection error: %s\n", c2->errstr);
	redisFree(c2);
} else
{
	printf("Connection error: can't allocate redis context\n");
}
exit(1);
}
}

void REDIS::Writeredisdiscovery(std::string hash_key, std::string discoveryinfo)
{

        REDIS hash;
        hash.Context();
        redisReply *reply1;

        string res1 = "HMSET "+ hash_key + space + discoveryinfo;
        int TempNumOne=res1.size();
        char res[1000];
        for (int a=0; a<=TempNumOne; a++)
        {
                res[a]=res1[a];
        }
        reply1= (redisReply *)redisCommand(c2,res);
        std::cout <<"\n" << res << endl;
        printf("HMSET :: %s\n",reply1->str);
        freeReplyObject(reply1);
	redisFree(c2);
}

void REDIS::Writeredisget(std::string hash_key, const OCRepresentation& rep)
{
        REDIS hash;
        std::string key = "status_" + hash_key;

        OCRepPayloadValue* repp;
        repp = rep.getPayload()->values;

                while(repp) {

                        stringstream stringAtrVal;
                        std::string buffer = "";
                        redisReply *reply1; //redis Message
                        hash.Context(); //redis connectio to write in to db
                        int datatype = repp->type;
                        char* names = repp->name;
                        std::string name(names);

                        RepPayloadMap.insert ( std::pair<std::string, int > ( name, datatype ) );

                        cout << "DataType :" << datatype <<endl;
                        cout << "AttrName:" << name <<endl;
                        switch (datatype)
                        {
                                cout << "\n\n\tSWITCH\n " <<endl;
                                case 0:
                                cout << "OCREP_PROP_NULL " <<endl;
                                break;

                                case 1:
                        {
                                cout << " :: OCREP_PROP_INT :: " <<endl;
                                cout << "AttrName :" << name <<endl;
                                cout << "AttrValue:" << repp->i <<endl;
                                stringAtrVal << repp->i;
                                string str = stringAtrVal.str();

                                buffer =  name + space + str;
                        }
                        break;

                        case 2:
                        {
                                cout << "OCREP_PROP_DOUBLE  " <<endl;
                                cout << "AttrName :" << name <<endl;
                                cout << "AttrValue:" << repp->d <<endl;

                                stringAtrVal << repp->d;
                                string str = stringAtrVal.str();

                                buffer =  name + space + str;
                        }
                        break;

                        case 3:
                                {
                                cout << "OCREP_PROP_BOOL  " <<endl;
                                cout << "AttrName :" << name <<endl;
                                cout << "AttrValue:" << boolalpha << repp->b <<endl;

                                stringAtrVal << repp->b;
                                string str = stringAtrVal.str();

                                buffer =  name + space + str;

                        }
                        break;

                        case 4:
                        {
                                cout << "OCREP_PROP_STRING  " <<endl;
                                cout << "AttrName :" << name <<endl;
                                cout << "AttrValue:" << repp->str <<endl;
                                stringAtrVal << repp->str;
                                string str = stringAtrVal.str();
                                buffer =  name + space + str;

                                }
                        break;

                        case 5:
                                cout<<"OCREP_PROP_BYTE_STRING"<<endl;
                        break;
                        case 6:
                                cout<<"OCREP_PROP_OBJECT"<<endl;
                        break;
                        case 7:
                                cout<<"OCREP_PROP_ARRAY"<<endl;
                                rep.getValue(repp->name, chromargbArr);
                                std::cout <<"\t"<< repp->name<<"\n";
                                for(int s : chromargbArr) {
                                std::cout <<"\n\tjustprint      ["<< s <<"]\n";
                                }
                                rep.getValue(repp->name, chromargbArr);
                                Json::Value threearray;
                                for(int count = 0; count < chromargbArr.size(); count++ ) {

                                threearray[count] = chromargbArr[count];

                                }
                                stringAtrVal << threearray[0] <<"," << threearray[1] <<  "," << threearray[2];
                                string str = stringAtrVal.str();
                                buffer =  name + space + str;

                        break;

                        }

                                        string res1 = "HMSET "+ key + space + buffer;
                                        int TempNumOne=res1.size();
                                        char res[1000];
                                        for (int a=0; a<=TempNumOne; a++)
                                        {
                                        res[a]=res1[a];
                                        }

                                        reply1= (redisReply *)redisCommand(c2,res);
                                        std::cout <<"\n" << res << endl;
                                        printf("HMSET :: %s\n",reply1->str);
                                        printf("\nIPADDRESS of DB :: %s\n",hostname);

                                        freeReplyObject(reply1);
                                        redisFree(c2);

                                        repp = repp->next;

                }         

}
