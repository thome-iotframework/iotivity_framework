#include <stdlib.h>
#include <iostream>
#include <jsonstructure.hpp>

using namespace std;

/*-------------JsonCPP-------------*/
#include <fstream>
#include <json/json.h>
/*-------------JsonCPP-------------*/


JsonParsestruct jsonReader(std::string jsoninput)
{
        JsonParsestruct JP;

        Json::Value root;
        Json::Reader reader;
        reader.parse( jsoninput.c_str(), root ); //parse process
        JP.command = root["command"].asString();
        JP.UUID = root["uuid"].asString();
        JP.DevID = root["devId"].asString();
        JP.attributeName1 = root["attributeName1"].asString();
        JP.attributeValue1 = root["attributeValue1"].asString();

        return JP;

}
