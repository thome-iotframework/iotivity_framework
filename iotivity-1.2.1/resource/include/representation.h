#include <iostream>
#include <string>

using namespace std;
using namespace OC;


//t_things devices;
extern std::map<std::string,int>RepPayloadMap;                     // tthings representation map

void representation(const OCRepresentation& rep){

	cout << "In function representation" << endl;
		cout << "Host Address: " << rep.getHost() << endl;
		cout << "No. of attributes: " << rep.numberOfAttributes() << endl;
		OCRepPayload *repPayload;
		repPayload = rep.getPayload();

		while (repPayload){

			cout << "Resource URI from represenation: " << repPayload -> uri << endl;
			OCRepPayloadValue * repPayloadValue;
			repPayloadValue = repPayload -> values;

			while (repPayloadValue){
				cout << "Attribute Name: " << repPayloadValue -> name << endl;
				cout << "Attribute Type: " << repPayloadValue -> type << endl;
				std::string attributeName_string(repPayloadValue -> name);

				RepPayloadMap.insert(pair<string,int>(attributeName_string,repPayloadValue -> type));

				switch(repPayloadValue->type){

					case 0:
						cout<<"OCREP_PROP_NULL"<<endl;
					break;
					case 1:
						cout<<"OCREP_PROP_INT"<<endl;
						cout << "Attribute Value: " << repPayloadValue -> i << endl;
					break;
					case 2:
						cout<<"OCREP_PROP_DOUBLE"<<endl;
						cout << "Attribute Value: " << repPayloadValue -> d << endl;
					break;
					case 3:
						cout<<"OCREP_PROP_BOOL"<<endl;
						cout << "Attribute Value: " << repPayloadValue -> b << endl;
					break;
					case 4:
						cout<<"OCREP_PROP_STRING"<<endl;
						cout << "Attribute Value: " << repPayloadValue -> str << endl;
					break;
					case 5:
						cout<<"OCREP_PROP_BYTE_STRING"<<endl;
					break;
					case 6:
						cout << "OCREP_PROP_OBJECT" << endl;
						OCRepPayload* objectPayload;
						objectPayload = repPayloadValue -> obj;

						while (objectPayload){

								cout << "Resource URI from Object: " << objectPayload -> uri << endl;
								OCRepPayloadValue* objectPayloadValue;
								objectPayloadValue = objectPayload -> values;

								while (objectPayloadValue){
										cout << "Attribute Name (Object): " << objectPayloadValue -> name << endl;
										cout << "Attribute Type	(Object): " << objectPayloadValue -> type << endl;

			RepPayloadMap.insert(pair<string,int>(attributeName_string,repPayloadValue -> type));

										switch (objectPayloadValue -> type){
											case 0:
												cout<<"OCREP_PROP_NULL"<<endl;
											break;
											case 1:
												cout<<"OCREP_PROP_INT"<<endl;
												cout << "Attribute Value: " << objectPayloadValue -> i << endl;
											break;
											case 2:
												cout<<"OCREP_PROP_DOUBLE"<<endl;
												cout << "Attribute Value: " << objectPayloadValue -> d << endl;
											break;
											case 3:
												cout<<"OCREP_PROP_BOOL"<<endl;
												cout << "Attribute Value: " << objectPayloadValue -> b << endl;
											break;
											case 4:
												cout<<"OCREP_PROP_STRING"<<endl;
												cout << "Attribute Value: " << objectPayloadValue -> str << endl;
											break;
											case 5:
												cout<<"OCREP_PROP_BYTE_STRING"<<endl;
											break;
											case 6:
												cout<<"OCREP_PROP_OBJECT"<<endl;
											break;
											case 7:
												cout<<"OCREP_PROP_ARRAY(repObject)"<<endl;
											break;
											}
										objectPayloadValue = objectPayloadValue -> next;
									}
							objectPayload = objectPayload -> next;
							}

					break;
					case 7:
						cout<<"OCREP_PROP_ARRAY(repPayload)"<<endl;
					break;
					}
				repPayloadValue = repPayloadValue -> next;
			}
		repPayload = repPayload -> next;
	}
}
