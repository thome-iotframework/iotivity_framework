
#ifndef DISCOVERY_H_
#define DISCOVERY_H_


  void foundResource (std::shared_ptr<OCResource> resource);
  void discoveryFun ();
  void receivedDeviceInfo (const OCRepresentation& rep);
  void deviceInfo ();
  void receivedPlatformInfo (const OCRepresentation& rep);
  void platformInfo ();
  void Discovery_all ();
  void t_things_garbage ();
  void t_things_device ();
  void t_resource_attPrint (OCRepPayloadValue* repp);
  void t_things_print ();
  void t_resource_att_details ();
  void end_Discovery (int sig);

  
#endif /* DISCOVERY_H_ */
