/*
 * device_thing_class.h
 *
 *  Created on: 09-Jun-2017
 *      Author: smartron
 */

#ifndef DEVICE_THING_CLASS_H_
#define DEVICE_THING_CLASS_H_

using namespace OC;
using namespace std;


struct attributetype
{
        int i;
        double d;
        bool b;
        string str;
};

class t_resource
{
public:
std::string resourceURI;
std::string serverID;
std::string key;

std::shared_ptr<OCResource> resource;
OCRepPayloadValue* Attdetals;

void onGet(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
void getRepresentation( );
void onPost(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
void postRepresentation( OCRepresentation rep);
void onObserve(const HeaderOptions /*headerOptions*/, const OCRepresentation& rep,
               const int& eCode, const int& sequenceNumber);
void observeRepresentation( );
};

class t_device
{
public:
string Coluri;
std::map<string, t_resource*> resMap;
};

class t_things
{
public:
string thingsName;
string thingsType;

string uuid;
string manufacturerName;
string osName;
string firmwareVersion;
string hardwareVersion;

std::vector<t_device*> deviceVec;
};



#endif /* #define DEVICE_THING_CLASS_H_ */
